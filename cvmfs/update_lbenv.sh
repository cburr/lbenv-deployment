#!/bin/bash
###############################################################################
# (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

BASE_URL=https://lhcb-rpm.web.cern.ch/lhcb-rpm/lbenv-kits/lhcb.cern.ch
INSTALL_ROOT=/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv

new_sha1=$(curl -s $BASE_URL/latest.txt | sha1sum)
old_sha1=$(cat $INSTALL_ROOT/.installed_kits.txt | sha1sum)

if [ "$new_sha1" != "$old_sha1" ] ; then
    # FIXME: ask for confirmation/check if a transaction was started...
    ref=.installed_kits.txt.$(date -Is)
    curl -s -o ${ref} $BASE_URL/latest.txt

    # this is the old trick of getting lines in file b.txt but not in file a.txt:
    #   cat a.txt a.txt b.txt | sort | uniq -u
    # refactored as
    #   sort a.txt{,} b.txt | uniq -u
    new_files=$(sort $INSTALL_ROOT/.installed_kits.txt{,} ${ref} | uniq -u)

    for t in ${new_files} ; do
        echo "Unpacking $t ..."
        flavour=$(echo $t | cut -d. -f2)
        host_os=$(echo $t | cut -d. -f3)
        build_id=$(echo $t | cut -d. -f4 | cut -d- -f1)

        if [ "$flavour" = "prod" ] ; then
            link=pre-prod
        else
            link=$flavour
        fi

        curl -s $BASE_URL/$t | tar xjfC - /
        rm -rf $INSTALL_ROOT/$link/$host_os
        mkdir -p $INSTALL_ROOT/$link
        ln -sv ../$build_id/$flavour/$host_os $INSTALL_ROOT/$link/$host_os
    done

    mv -v ${ref} $INSTALL_ROOT/${ref}
    ln -svf ${ref} $INSTALL_ROOT/.installed_kits.txt
else
    echo "nothing to update"
fi
