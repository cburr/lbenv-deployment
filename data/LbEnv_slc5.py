###############################################################################
# (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from string import Template

SHELL_SCRIPT = {
    'sh':
    Template('''
export PATH=$siteroot/lcg/external/Python/2.7.3/x86_64-slc5-gcc46-opt/bin:$PATH ;
source $siteroot/lhcb/LBSCRIPTS/$flavour/InstallArea/scripts/LbLogin.sh -m $siteroot $opts
'''),
    'csh':
    Template('''
set path = ($siteroot/lcg/external/Python/2.7.3/x86_64-slc5-gcc46-opt/bin $path) ;
source $siteroot/lhcb/LBSCRIPTS/$flavour/InstallArea/scripts/LbLogin.csh -m $siteroot $opts
''')
}


def main():
    import sys

    from optparse import OptionParser
    parser = OptionParser(prog='LbEnv')
    parser.add_option(
        '-c', '--platform', help='override the default platform detection')
    parser.add_option('-r', '--siteroot', help='path to the installation root')
    parser.add_option(
        "--sh",
        action="store_const",
        const="sh",
        dest="shell",
        help="print the changes to the environment as shell "
        "commands for 'sh'-derived shells")
    parser.add_option(
        "--csh",
        action="store_const",
        const="csh",
        dest="shell",
        help="print the changes to the environment as shell "
        "commands for 'csh'-derived shells")
    parser.set_defaults(shell='sh')

    if set(['-h', '--help']).intersection(sys.argv):
        # if we are in shell script mode, and want the help message, it should
        # be printed on stderr
        sys.stdout = sys.stderr

    opts, args = parser.parse_args()

    if opts.platform:
        extra_opts = '-c ' + opts.platform
    else:
        extra_opts = ''

    if '/unstable/' in __file__:
        flavour = 'dev'
    else:
        flavour = 'prod'

    print SHELL_SCRIPT[opts.shell].safe_substitute(
        siteroot=opts.siteroot,
        flavour=flavour,
        opts=extra_opts)


if __name__ == '__main__':
    main()
