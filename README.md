# lbenv-deployment
This project host tools and configuration files needed to help deploying LHCb
utilities and environment scripts (see [LbEnv](https://gitlab.cern.ch/lhcb-core/LbEnv)).

In particular we produce here the deployment kits for the tools (i.e. archives
containing a prebuilt virtualenv directory) and the RPM to control the default
versions of the external utilities (contrib).


# Structure
## data
The directory [`data`](data) contains the files dictating which Python packages
to install for the _unstable_ (cutting edge) or _stable_ (pre-production) kits:
- [data/unstable-requirements.txt.in](data/unstable-requirements.txt.in)
- [data/stable-requirements.txt.in](data/stable-requirements.txt.in)

It also contains example entry scripts to set up the run/development-time
environment:

Since SLC5 is not supported by the latest version of the tools, we provide also
helpers to transparently switch to the legacy version of the tools
([LbScripts](https://gitlab.cern.ch/lhcb-core/LbScripts)).

### data/clone_venv
[`clone_venv`](data/clone_venv) is a script that can be used to clone an
installed kit to a writable location to allow debugging and development.

The script is usually installed in the directory where the kit is deployed, and
should be used from there, as, for example, in this workflow:
```sh
cd ~/workspace
export MYSITEROOT=/cvmfs/lhcb.cern.ch/lib
git clone ssh://git@gitlab.cern.ch:7999/lhcb-core/LbEnv.git
${MYSITEROOT}/var/lib/LbEnv/dev/x86_64-slc6/clone_venv env-test
unset LBENV_SOURCED
source env-test/bin/LbEnv.sh
pip install -e ./LbEnv
```


## rpm
The version of the contrib pacakges to install and the creation of the symlinks
required to make them available in the LHCb environment is delegated to an RPM
package which _spec_ file is hosted in the [`rpm`](rpm) directory.

The RPM is built via a Gitlab-CI job (see [.gitlab-ci.yml](.gitlab-ci.yml)).


## jenkins
The actual creation of the deployment kits is performed by a Jenkins job
([core-soft-tasks/lbenv-deployment](https://jenkins-lhcb-nightlies.web.cern.ch/job/core-soft-tasks/job/lbenv-deployment/)),
via the scripts in the directory [`jenkins`](jenkins).

### jenkins/generate_kits
Entry point for the Jenkins job that generate the deployment kits.

Prepare the Python environment to run `jenkins/_generate_kits`.

### jenkins/_generate_kits
Python script to actually generate the kits.

For each pair _flavour_ (_stable_, _unstable_) and _platform_ (supported platforms),
call `jenkins/prepare_kit` in the appropriate container.

### jenkins/prepare_kit
Called by `jenkins/_generate_kits` to actuall create the deployment kit.

`prepare_kit` is meant to be run in the target system for the deployment kit.

Usage: `CMTCONFIG=<platform> jenkins/prepare_kit <installation_prefix> <flavour>`

where `<flavour>` can be _stable_ or _unstable_, and `<platform>` is the platform id
of the target system.


## cvmfs
The script [`cvmfs/update_lbenv.sh`](cvmfs/update_lbenv.sh) is used on the
production CVMFS Stratum-0 to update the installed kits.


## _misc_

### basic_install.sh
This scripts is a working example of how to bootstrap a local installation of
the environment tools.

Usage:
```sh
curl -O https://gitlab.cern.ch/lhcb-core/lbenv-deployment/raw/master/basic_install.sh
bash basic_install.sh <installation_prefix> [flavour]
```


# Conventions
The deployment kits are named `lbenv-kit.<flavour>.<host-os>.<build-id>-<checksum>.tar.bz2`, where:

- `<flavour>` is any of _stable_ or _unstable_
- `<host-os>` is a string identifying the architecture and OS version the kit is built for
- `<build-id>` an increasing number to distinguish different builds
- `<checksum>` a cryptographic checksum of the list and versions of Python packages installed in the kit


The kits are hosted in a directory on EOS with the same name of the CVMFS volume
they are targeting (at the moment only `lhcb.cern.ch`).

The _virtualenv_ directories in the kits are non-rilocatable and produced to be
installed in `<root>/var/lib/LbEnv/<build-id>/<flavour>/<host-os>`, where, in
the case of `/cvmfs/lhcb.cern.ch`, `<root>` is `/cvmfs/lhcb.cern.ch/lib`.  For
simplicity, the tar files contain the full path of the target directory
(including the leading `/`).


# Deployment workflow
## Kits
An update of the _master_ branch of this project or of the content of the
[LHCb PyPI server](https://cern.ch/lhcb-pypi) triggers a build of the
[Jenkins job](https://jenkins-lhcb-nightlies.web.cern.ch/job/core-soft-tasks/job/lbenv-deployment/),
which will try to produce new versions of the deployment kits. If there are any
changes with respect to the kits already uploaded to the [EOS archive](https://lhcb-rpm.web.cern.ch/lhcb-rpm/lbenv-kits/lhcb.cern.ch)
the new kits are uploaded and the special file [latest.txt](https://lhcb-rpm.web.cern.ch/lhcb-rpm/lbenv-kits/lhcb.cern.ch/latest.txt) is updated.

On CVMFS Stratum-0, the script [`cvmfs/update_lbenv.sh`](cvmfs/update_lbenv.sh)
is called to compare the currently installed kits with the latest versions on
EOS, and downloads and unpacks in the target directory the new kits. Then it
updated the special symlinks `<root>/var/lib/LbEnv/<flavour>/<host-os>` to point to
the the latest installed version, but using the special flavour _testing_
instead of _stable_ to allow for a validation of the production installation before
the actual switch (_unstable_ is directly updated, instead).

## Contrib
At every new commit pushed to the Gitlab server, a Gitlab-CI job compares
version and release numbers in [`rpm/ContribSelection.spec`](rpm/ContribSelection.spec)
with the RPMs in [https://lhcb-rpm.web.cern.ch/lhcb-rpm/incubator](https://lhcb-rpm.web.cern.ch/lhcb-rpm/incubator),
and, if not present, builds the new RPM and deploys it to EOS.
