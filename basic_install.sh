#!/bin/bash
###############################################################################
# (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

python_version=$(python -c 'import sys; print "%s.%s" % sys.version_info[:2]')

script_location=$(cd $(dirname $0) && pwd)
if [ -z "$script_location" ] ; then
  echo "error cannt find myself, invoke using the absolute path"
  exit 1
fi

if [ -z "$1" ] ; then
  echo "usage: $(basename $0) installation_prefix [flavour]"
  exit 1
fi

case "$python_version" in
  2.7 | 3.5 | 3.6 | 3.7 ) ;; # supported version
  *)
   echo "Your version of Python ($python_version) is not supported, please upgrade"
   #exit 1
   ;;
esac

if ! which virtualenv > /dev/null 2>&1 ; then
  echo "Please install virtualenv"
  exit 1
fi

virtualenv $1
. $1/bin/activate

# ensure we have the latest version of pip
pip install --upgrade pip setuptools pip-tools

# enable LHCb PyPI server by default and copy the requirements
if [ -e "$script_location/data" ] ; then
  cp $script_location/data/pip.conf  $script_location/data/clone_venv $1
  pip-compile --output-file $1/.requirements.txt $script_location/data/${2:-stable}-requirements.txt.in >/dev/null
  cp $script_location/data/LbEnv.sh $script_location/data/LbEnv.csh $1/bin
else
  curl -o $1/pip.conf https://gitlab.cern.ch/lhcb-core/lbenv-deployment/raw/master/data/pip.conf
  curl -o $1/clone_venv https://gitlab.cern.ch/lhcb-core/lbenv-deployment/raw/master/data/clone_venv
  chmod a+x $1/clone_venv
  curl -o $1/.requirements.txt https://gitlab.cern.ch/lhcb-core/lbenv-deployment/raw/master/data/${2:-stable}-requirements.txt.in
  pip-compile $1/.requirements.txt >/dev/null
  curl -o $1/bin/LbEnv.sh https://gitlab.cern.ch/lhcb-core/lbenv-deployment/raw/master/data/LbEnv.sh
  curl -o $1/bin/LbEnv.csh https://gitlab.cern.ch/lhcb-core/lbenv-deployment/raw/master/data/LbEnv.csh
fi

# FIXME: workaround for an issue with pip-compile
sed -i 's|^--index-url.*|\0\n--extra-index-url https://pypi.org/simple|' $1/.requirements.txt

# install
pip install -r $1/.requirements.txt

# fix entry point scripts
sed -i "s&%PREFIX%&$1&g" $1/bin/LbEnv.sh $1/bin/LbEnv.csh
