#!/bin/bash -ex
###############################################################################
# (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

rm -rf contrib kits dist data/*-requirements.txt
mkdir -p contrib

# make sure that eosmount works
systemctl status eosd | grep -q eosproject || sudo systemctl restart eosd

curl https://gitlab.cern.ch/lhcb-core/LbDocker/raw/master/scripts/lb-docker-run -o contrib/lb-docker-run
chmod a+x contrib/lb-docker-run

curl https://files.pythonhosted.org/packages/b1/72/2d70c5a1de409ceb3a27ff2ec007ecdd5cc52239e7c74990e32af57affe9/virtualenv-15.2.0.tar.gz | \
  tar xzfC - contrib
ln -s virtualenv-15.2.0 contrib/virtualenv

python contrib/virtualenv/virtualenv.py contrib/tools
source contrib/tools/bin/activate

pip install pip-tools

for requirements in data/*-requirements.txt.in ; do
  pip-compile --rebuild --output-file ${requirements/.in/} ${requirements}
  # FIXME: workaround for an issue with pip-compile
  sed -i 's|^--index-url.*|\0\n--extra-index-url https://pypi.org/simple|' ${requirements/.in/}
  chmod 644 ${requirements/.in/}
done

$(dirname $(realpath $0))/_generate_kits
